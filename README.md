# Wagic the Homebrew

## Description

Wagic, the Homebrew, is a C++ game engine that allows to play card games against an AI on
- Android (phones and tablets) 
- Sony PSP
- Windows desktops 
- MacOS
- Linux

It is highly customizable and allows the player to tweak the rules / create their own cards, their own themes, etc... 


Info, downloads, discussions and more at http://wololo.net/forum/index.php

![alt text](http://wololo.net/wagic/wp-content/uploads/2009/10/shop.jpg "Screenshot")


### Sample round play-through video
[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/WUFSAPZuDIk/0.jpg)](http://www.youtube.com/watch?v=WUFSAPZuDIk)
