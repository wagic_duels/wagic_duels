//-------------------------------------------------------------------------------------
//
// JGE++ is a hardware accelerated 2D game SDK for PSP/Windows.
#include "JAssert.h"
#include "JFileSystem.h"
#include "JGE.h"
#include "JRenderer.h"
#include "JResourceManager.h"

JQuad::JQuad(JTexture* tex, float x, float y, float width, float height) : mTex(tex), mX(x), mY(y), mWidth(width), mHeight(height)
{
  JASSERT(tex != NULL);
  JRenderer::GetInstance()->TransferTextureToGLContext(*tex);

  mHotSpotX = 0.0f;
  mHotSpotY = 0.0f;
  // mBlend = BLEND_DEFAULT;
  for (int i = 0; i < 4; i++)
    mColor[i].color = 0xFFFFFFFF;

  mHFlipped = false;
  mVFlipped = false;

  SetTextureRect(x, y, width, height);
}

void JQuad::SetTextureRect(float x, float y, float w, float h)
{
  mX = x;
  mY = y;
  mWidth = w;
  mHeight = h;

  if (mTex)
  {
    mTX0 = x / mTex->mTexWidth;
    mTY0 = y / mTex->mTexHeight;
    mTX1 = (x + w) / mTex->mTexWidth;
    mTY1 = (y + h) / mTex->mTexHeight;
  }
}

void JQuad::GetTextureRect(float* x, float* y, float* w, float* h)
{
  *x = mX;
  *y = mY;
  *w = mWidth;
  *h = mHeight;
}

void JQuad::SetColor(PIXEL_TYPE color)
{
  for (int i = 0; i < 4; i++)
    mColor[i].color = color;
}

void JQuad::SetHotSpot(float x, float y)
{
  mHotSpotX = x;
  mHotSpotY = y;
}

//////////////////////////////////////////////////////////////////////////

JTexture::JTexture() : mWidth(0), mHeight(0), mBuffer(NULL) { mTexId = -1; }

JTexture::~JTexture()
{
  if (mBuffer)
  {
    delete[] mBuffer;
    mBuffer = NULL;
  }
}

void JTexture::UpdateBits(int, int, int, int, PIXEL_TYPE*) { JRenderer::GetInstance()->BindTexture(this); }

//////////////////////////////////////////////////////////////////////////

JRenderer* JRenderer::mInstance = NULL;
bool JRenderer::m3DEnabled = false;

void JRenderer::Set3DFlag(bool flag) { m3DEnabled = flag; }

JRenderer* JRenderer::GetInstance()
{
  if (mInstance == NULL)
  {
    mInstance = new JRenderer();

    JASSERT(mInstance != NULL);

    mInstance->InitRenderer();
  }

  return mInstance;
}

void JRenderer::Destroy()
{
  if (mInstance)
  {
    mInstance->DestroyRenderer();
    delete mInstance;
    mInstance = NULL;
  }
}

JRenderer::JRenderer() : mActualWidth(SCREEN_WIDTH_F), mActualHeight(SCREEN_HEIGHT_F) {}

JRenderer::~JRenderer() {}

void JRenderer::InitRenderer()
{
  mCurrentTextureFilter = TEX_FILTER_NONE;
  mImageFilter = NULL;

  mCurrTexBlendSrc = BLEND_SRC_ALPHA;
  mCurrTexBlendDest = BLEND_ONE_MINUS_SRC_ALPHA;

  //	mLineWidth = 1.0f;
  mCurrentTex = -1;
  mFOV = 75.0f;

#ifdef USING_MATH_TABLE
  for (int i = 0; i < 360; i++)
  {
    mSinTable[i] = sinf(i * DEG2RAD);
    mCosTable[i] = cosf(i * DEG2RAD);
  }
#endif

  mCurrentRenderMode = MODE_UNKNOWN;
}

void JRenderer::DestroyRenderer() {}
void JRenderer::BeginScene() {}
void JRenderer::EndScene() {}
void JRenderer::BindTexture(JTexture*) {}

void JRenderer::EnableTextureFilter(bool flag)
{
  if (flag)
    mCurrentTextureFilter = TEX_FILTER_LINEAR;
  else
    mCurrentTextureFilter = TEX_FILTER_NEAREST;

  mCurrentTex = -1;
}

void Swap(float* a, float* b)
{
  float n = *a;
  *a = *b;
  *b = n;
}

void JRenderer::RenderQuad(JQuad* quad, float, float, float, float, float)
{
  float width = quad->mWidth;
  float height = quad->mHeight;
  float x = -quad->mHotSpotX;
  float y = quad->mHotSpotY;

  Vector2D pt[4];
  pt[3] = Vector2D(x, y);
  pt[2] = Vector2D(x + width, y);
  pt[1] = Vector2D(x + width, y - height);
  pt[0] = Vector2D(x, y - height);

  Vector2D uv[4];
  uv[0] = Vector2D(quad->mTX0, quad->mTY1);
  uv[1] = Vector2D(quad->mTX1, quad->mTY1);
  uv[2] = Vector2D(quad->mTX1, quad->mTY0);
  uv[3] = Vector2D(quad->mTX0, quad->mTY0);

  if (quad->mHFlipped)
  {
    Swap(&uv[0].x, &uv[1].x);
    Swap(&uv[2].x, &uv[3].x);
  }

  if (quad->mVFlipped)
  {
    Swap(&uv[0].y, &uv[2].y);
    Swap(&uv[1].y, &uv[3].y);
  }

  BindTexture(quad->mTex);
}

void JRenderer::RenderQuad(JQuad* quad, VertexColor* pt)
{
  for (int i = 0; i < 4; i++)
  {
    pt[i].y = SCREEN_HEIGHT_F - pt[i].y;
    quad->mColor[i].color = pt[i].color;
  }

  Vector2D uv[4];
  uv[0] = Vector2D(quad->mTX0, quad->mTY1);
  uv[1] = Vector2D(quad->mTX1, quad->mTY1);
  uv[2] = Vector2D(quad->mTX1, quad->mTY0);
  uv[3] = Vector2D(quad->mTX0, quad->mTY0);

  BindTexture(quad->mTex);
}

void JRenderer::FillRect(float, float, float, float, PIXEL_TYPE) {}

void JRenderer::DrawRect(float, float, float, float, PIXEL_TYPE) {}

void JRenderer::FillRect(float x, float y, float width, float height, PIXEL_TYPE* colors)
{
  JColor col[4];
  for (int i = 0; i < 4; i++)
    col[i].color = colors[i];

  FillRect(x, y, width, height, col);
}

void JRenderer::FillRect(float, float, float, float, JColor*) {}
void JRenderer::DrawLine(float, float, float, float, PIXEL_TYPE) {}
void JRenderer::Plot(float, float, PIXEL_TYPE) {}
void JRenderer::PlotArray(float*, float*, int, PIXEL_TYPE) {}
void JRenderer::ScreenShot(const char*) {}

void JRenderer::TransferTextureToGLContext(JTexture&) {}

JTexture* JRenderer::CreateTexture(int, int, int)
{
  JTexture* tex = new JTexture();
  return tex;
}

JTexture* JRenderer::LoadTexture(const char*, int, int)
{
  JTexture* tex = new JTexture();
  return tex;
}

void JRenderer::EnableVSync(bool) {}
void JRenderer::ClearScreen(PIXEL_TYPE) {}
void JRenderer::SetTexBlend(int, int) {}
void JRenderer::SetTexBlendSrc(int) {}
void JRenderer::SetTexBlendDest(int) {}
void JRenderer::Enable2D() {}
void JRenderer::Enable3D() {}
void JRenderer::SetClip(int, int, int, int) {}
void JRenderer::LoadIdentity() {}
void JRenderer::Translate(float, float, float) {}
void JRenderer::RotateX(float) {}
void JRenderer::RotateY(float) {}
void JRenderer::RotateZ(float) {}
void JRenderer::PushMatrix() {}
void JRenderer::PopMatrix() {}
void JRenderer::RenderTriangles(JTexture*, Vertex3D*, int, int) {}
void JRenderer::SetFOV(float fov) { mFOV = fov; }
void JRenderer::FillPolygon(float*, float*, int, PIXEL_TYPE) {}
void JRenderer::DrawPolygon(float*, float*, int, PIXEL_TYPE) {}

void JRenderer::DrawLine(float x1, float y1, float x2, float y2, float lineWidth, PIXEL_TYPE color)
{
  float dy = y2 - y1;
  float dx = x2 - x1;
  if (dy == 0 && dx == 0) return;

  float l = (float)hypot(dx, dy);

  float x[4];
  float y[4];

  x[0] = x1 + lineWidth * (y2 - y1) / l;
  y[0] = y1 - lineWidth * (x2 - x1) / l;

  x[1] = x1 - lineWidth * (y2 - y1) / l;
  y[1] = y1 + lineWidth * (x2 - x1) / l;

  x[2] = x2 - lineWidth * (y2 - y1) / l;
  y[2] = y2 + lineWidth * (x2 - x1) / l;

  x[3] = x2 + lineWidth * (y2 - y1) / l;
  y[3] = y2 - lineWidth * (x2 - x1) / l;

  FillPolygon(x, y, 4, color);
}

void JRenderer::DrawCircle(float, float, float, PIXEL_TYPE) {}
void JRenderer::FillCircle(float, float, float, PIXEL_TYPE) {}
void JRenderer::DrawPolygon(float, float, float, int, float, PIXEL_TYPE) {}
void JRenderer::FillPolygon(float, float, float, int, float, PIXEL_TYPE) {}
void JRenderer::SetImageFilter(JImageFilter* imageFilter) { mImageFilter = imageFilter; }
void JRenderer::DrawRoundRect(float, float, float, float, float, PIXEL_TYPE) {}
void JRenderer::FillRoundRect(float, float, float, float, float, PIXEL_TYPE) {}
