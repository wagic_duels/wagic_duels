#include <wge/thread/psp_thread.hxx>

#include <pspthreadman.h>
#include <pthread.h>

#include <system_error>

namespace wge
{
  namespace
  {

    extern "C" void* execute_native_thread_routine(void* funct_ptr)
    {
      thread::impl_base* base_ptr = static_cast<thread::impl_base*>(funct_ptr);
      thread::shared_base_type local_ptr;
      local_ptr.swap(base_ptr->m_this_ptr);
      base_ptr->run();
      return nullptr;
    }

  } // namespace

  inline void thread::start_thread(shared_base_type base_ptr)
  {
    base_ptr->m_this_ptr = base_ptr;
    int error_code = pthread_create(&m_thread_id.m_thread, nullptr, &execute_native_thread_routine, base_ptr.get());
    if (error_code)
    {
      base_ptr->m_this_ptr.reset();
      throw std::system_error(std::error_code(error_code, std::system_category()));
    }
  }

  void thread::join()
  {
    int error_code = EINVAL;
    if (joinable()) error_code = pthread_join(m_thread_id.m_thread, nullptr);

    if (error_code)
    {
      throw std::system_error(std::error_code(error_code, std::system_category()));
    }

    m_thread_id = id();
  }

  void thread::detach()
  {
    int error_code = EINVAL;

    if (joinable()) error_code = pthread_detach(m_thread_id.m_thread);

    if (error_code)
    {
      throw std::system_error(std::error_code(error_code, std::system_category()));
    }

    m_thread_id = id();
  }

  namespace this_thread
  {
    void sleep_for(time::seconds s, time::nanoseconds ns)
    {
      auto sleep_time = time::milliseconds(s).count() + (ns.count() / 1000);
      ::sceKernelDelayThread(sleep_time);
    }

  } // namespace this_thread

} // namespace wge