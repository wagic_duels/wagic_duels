cmake_minimum_required(VERSION 3.1)

if(PSP)
    add_definitions(-DPSP_WGE)
endif()

add_subdirectory(extern EXCLUDE_FROM_ALL)

set(WGE_SRCS)

if( PSP )
    set(WGE_SRCS "${CMAKE_CURRENT_SOURCE_DIR}/src/thread/psp_thread.cxx")
    add_library(wge STATIC ${WGE_SRCS})
    target_link_libraries(wge PUBLIC pthread-psp)
    target_link_libraries(wge PUBLIC TFM::tinyformat)
    target_include_directories(wge PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
    target_compile_features(wge PUBLIC cxx_std_11)
else()
    add_library(wge INTERFACE)
    target_include_directories(wge INTERFACE "${CMAKE_CURRENT_SOURCE_DIR}/include")
    target_link_libraries(wge INTERFACE TFM::tinyformat)
    target_compile_features(wge INTERFACE cxx_std_11)
endif()

add_library(WGE::WGE ALIAS wge)

add_subdirectory(tests)