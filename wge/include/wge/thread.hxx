#ifndef WGE_THREAD_HXX
#define WGE_THREAD_HXX

#if defined WGE_PSP
#include <wge/thread/psp_thread.hxx>
#else
#include <thread>

namespace wge
{
  using std::thread;
  namespace this_thread = std::this_thread;
} // namespace wge

#endif // WGE_PSP
#endif // WGE_THREAD_HXX