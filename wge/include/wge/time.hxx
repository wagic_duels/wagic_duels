#ifndef WGE_TIME_HXX
#define WGE_TIME_HXX

#include <chrono>

#include <wge/types.hxx>

namespace wge
{
  namespace time
  {
    using std::chrono::duration;
    using std::chrono::duration_cast;
    using std::chrono::time_point;

    using nanoseconds = duration<wge::int64_t, std::nano>;
    using microseconds = duration<wge::int64_t, std::micro>;
    using milliseconds = duration<wge::int64_t, std::milli>;
    using seconds = duration<wge::int64_t>;
    using minutes = duration<wge::int64_t, std::ratio<60>>;
    using hours = duration<wge::int64_t, std::ratio<3600>>;

    namespace literals
    {

      constexpr milliseconds operator""_ms(wge::uint64_t ms) { return milliseconds(ms); }
      constexpr duration<long double, std::milli> operator""_ms(long double ms) { return duration<long double, std::milli>(ms); }

      constexpr seconds operator""_s(wge::uint64_t s) { return seconds(s); }
      constexpr duration<long double> operator""_s(long double s) { return duration<long double>(s); }

    } // namespace literals
  }   // namespace time

} // namespace wge

#endif // WGE_TIME_HXX