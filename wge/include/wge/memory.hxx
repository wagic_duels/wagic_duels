#ifndef WGE_MEMORY_HXX
#define WGE_MEMORY_HXX

#include <memory>
#include <type_traits>

namespace wge
{
  namespace detail
  {

    using std::shared_ptr;
    using std::unique_ptr;

    using std::make_shared;
#if __cplusplus > 201103L
    using std::make_unique;
#else
    namespace internal
    {
      template <typename T>
      struct make_unique
      {
        using single_object = unique_ptr<T>;
      };

      template <typename T>
      struct make_unique<T[]>
      {
        using array = unique_ptr<T[]>;
      };

      template <typename T, size_t Bound>
      struct make_unique<T[Bound]>
      {
        struct invalid_type
        {
        };
      };

      template <typename T>
      using remove_extent_t = typename std::remove_extent<T>::type;

      template <typename T>
      using remove_all_extents_t = typename std::remove_all_extents<T>::type;

    } // namespace internal

    template <typename T, typename... Args>
    inline typename internal::make_unique<T>::single_object make_unique(Args&&... args)
    {
      return unique_ptr<T>(new T(std::forward<Args>(args)...));
    }

    template <typename T>
    inline typename internal::make_unique<T>::array make_unique(std::size_t num)
    {
      return unique_ptr<T>(new internal::remove_extent_t<T>[num]());
    }

    template <typename T, typename... Args>
    inline typename internal::make_unique<T>::invalid_type make_unique(Args&&...) = delete;
#endif

  } // namespace detail
} // namespace wge

namespace wge
{
  using detail::shared_ptr;
  using detail::unique_ptr;

  using detail::make_shared;
  using detail::make_unique;

  template <class T, class = typename std::enable_if<std::is_pointer<T>::value>::type>
  using owner_ptr = T;
} // namespace wge

#endif /*WGE_MEMORY_HXX*/