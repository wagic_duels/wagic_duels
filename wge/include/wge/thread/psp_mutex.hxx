#ifndef WGE_THREAD_PSP_MUTEX_HXX
#define WGE_THREAD_PSP_MUTEX_HXX

// #include <chrono>
// #include <type_traits>
// #include <functional>
// #include <bits/functexcept.h>
// #include <bits/gthr.h>
// #include <bits/move.h> // for std::swap
// #include <bits/cxxabi_forced.h>

#include <wge/time.hxx>
#include <wge/utility.hxx>

#include <exception>
#include <system_error>
#include <tuple>

#include <pthread.h>

namespace wge
{

  namespace detail
  {
    class mutex_base
    {
    protected:
      using native_type = pthread_mutex_t;
      native_type m_mutex;

      mutex_base() noexcept { pthread_mutex_init(&m_mutex, nullptr); }
      ~mutex_base() noexcept { pthread_mutex_destroy(&m_mutex); }

      mutex_base(const mutex_base&) = delete;
      mutex_base& operator=(const mutex_base&) = delete;
    };

    class recursive_mutex_base
    {
    protected:
      using native_type = pthread_mutex_t;
      native_type m_mutex;

    private:
      static inline int mutex_init_function(native_type* __mutex)
      {
        pthread_mutexattr_t __attr;
        int error_code = pthread_mutexattr_init(&__attr);
        if (!error_code) error_code = pthread_mutexattr_settype(&__attr, PTHREAD_MUTEX_RECURSIVE);
        if (!error_code) error_code = pthread_mutex_init(__mutex, &__attr);
        if (!error_code) error_code = pthread_mutexattr_destroy(&__attr);
        return error_code;
      }

    protected:
      recursive_mutex_base() { mutex_init_function(&m_mutex); }
      ~recursive_mutex_base() noexcept { pthread_mutex_destroy(&m_mutex); }

      recursive_mutex_base(const recursive_mutex_base&) = delete;
      recursive_mutex_base& operator=(const recursive_mutex_base&) = delete;
    };

  } // namespace detail

  class mutex : private detail::mutex_base
  {
  public:
    using native_handle_type = native_type*;

    mutex() noexcept = default;
    ~mutex() = default;

    mutex(const mutex&) = delete;
    mutex& operator=(const mutex&) = delete;

    inline void lock()
    {
      int error_code = pthread_mutex_lock(&m_mutex);
      if (error_code)
      {
        throw std::system_error(std::error_code(error_code, std::system_category()));
      }
    }

    inline bool try_lock() noexcept { return !pthread_mutex_trylock(&m_mutex); }
    inline void unlock() noexcept { pthread_mutex_unlock(&m_mutex); }

    native_handle_type native_handle() { return &m_mutex; }
  };

  class recursive_mutex : private detail::recursive_mutex_base
  {
  public:
    using native_handle_type = native_type*;

    recursive_mutex() = default;
    ~recursive_mutex() = default;

    recursive_mutex(const recursive_mutex&) = delete;
    recursive_mutex& operator=(const recursive_mutex&) = delete;

    void lock()
    {
      int error_code = pthread_mutex_lock(&m_mutex);
      if (error_code)
      {
        throw std::system_error(std::error_code(error_code, std::system_category()));
      }
    }

    inline bool try_lock() noexcept { return !pthread_mutex_trylock(&m_mutex); }
    inline void unlock() noexcept { pthread_mutex_unlock(&m_mutex); }

    native_handle_type native_handle() { return &m_mutex; }
  };

  // #if _GTHREAD_USE_MUTEX_TIMEDLOCK
  //   template <typename _Derived>
  //   class __timed_mutex_impl
  //   {
  //   protected:
  //     typedef chrono::high_resolution_clock __clock_t;

  //     template <typename _Rep, typename _Period>
  //     bool _M_try_lock_for(const chrono::duration<_Rep, _Period>& __rtime)
  //     {
  //       using chrono::steady_clock;
  //       auto __rt = chrono::duration_cast<steady_clock::duration>(__rtime);
  //       if (ratio_greater<steady_clock::period, _Period>()) ++__rt;
  //       return _M_try_lock_until(steady_clock::now() + __rt);
  //     }

  //     template <typename _Duration>
  //     bool _M_try_lock_until(const chrono::time_point<__clock_t, _Duration>& __atime)
  //     {
  //       auto __s = chrono::time_point_cast<chrono::seconds>(__atime);
  //       auto __ns = chrono::duration_cast<chrono::nanoseconds>(__atime - __s);

  //       __gthread_time_t __ts = {static_cast<std::time_t>(__s.time_since_epoch().count()), static_cast<long>(__ns.count())};

  //       auto __mutex = static_cast<_Derived*>(this)->native_handle();
  //       return !__gthread_mutex_timedlock(__mutex, &__ts);
  //     }

  //     template <typename _Clock, typename _Duration>
  //     bool _M_try_lock_until(const chrono::time_point<_Clock, _Duration>& __atime)
  //     {
  //       auto __rtime = __atime - _Clock::now();
  //       return _M_try_lock_until(__clock_t::now() + __rtime);
  //     }
  //   };

  //   /// timed_mutex
  //   class timed_mutex : private __mutex_base, public __timed_mutex_impl<timed_mutex>
  //   {
  //   public:
  //     typedef native_type* native_handle_type;

  //     timed_mutex() = default;
  //     ~timed_mutex() = default;

  //     timed_mutex(const timed_mutex&) = delete;
  //     timed_mutex& operator=(const timed_mutex&) = delete;

  //     void lock()
  //     {
  //       int __e = __gthread_mutex_lock(&_M_mutex);

  //       // EINVAL, EAGAIN, EBUSY, EINVAL, EDEADLK(may)
  //       if (__e) __throw_system_error(__e);
  //     }

  //     bool try_lock() noexcept
  //     {
  //       // XXX EINVAL, EAGAIN, EBUSY
  //       return !__gthread_mutex_trylock(&_M_mutex);
  //     }

  //     template <class _Rep, class _Period>
  //     bool try_lock_for(const chrono::duration<_Rep, _Period>& __rtime)
  //     {
  //       return _M_try_lock_for(__rtime);
  //     }

  //     template <class _Clock, class _Duration>
  //     bool try_lock_until(const chrono::time_point<_Clock, _Duration>& __atime)
  //     {
  //       return _M_try_lock_until(__atime);
  //     }

  //     void unlock()
  //     {
  //       // XXX EINVAL, EAGAIN, EBUSY
  //       __gthread_mutex_unlock(&_M_mutex);
  //     }

  //     native_handle_type native_handle() { return &_M_mutex; }
  //   };

  //   /// recursive_timed_mutex
  //   class recursive_timed_mutex : private __recursive_mutex_base, public __timed_mutex_impl<recursive_timed_mutex>
  //   {
  //   public:
  //     typedef native_type* native_handle_type;

  //     recursive_timed_mutex() = default;
  //     ~recursive_timed_mutex() = default;

  //     recursive_timed_mutex(const recursive_timed_mutex&) = delete;
  //     recursive_timed_mutex& operator=(const recursive_timed_mutex&) = delete;

  //     void lock()
  //     {
  //       int __e = __gthread_recursive_mutex_lock(&_M_mutex);

  //       // EINVAL, EAGAIN, EBUSY, EINVAL, EDEADLK(may)
  //       if (__e) __throw_system_error(__e);
  //     }

  //     bool try_lock() noexcept
  //     {
  //       // XXX EINVAL, EAGAIN, EBUSY
  //       return !__gthread_recursive_mutex_trylock(&_M_mutex);
  //     }

  //     template <class _Rep, class _Period>
  //     bool try_lock_for(const chrono::duration<_Rep, _Period>& __rtime)
  //     {
  //       return _M_try_lock_for(__rtime);
  //     }

  //     template <class _Clock, class _Duration>
  //     bool try_lock_until(const chrono::time_point<_Clock, _Duration>& __atime)
  //     {
  //       return _M_try_lock_until(__atime);
  //     }

  //     void unlock()
  //     {
  //       // XXX EINVAL, EAGAIN, EBUSY
  //       __gthread_recursive_mutex_unlock(&_M_mutex);
  //     }

  //     native_handle_type native_handle() { return &_M_mutex; }
  //   };
  // #endif

  struct defer_lock_t
  {
  };

  struct try_to_lock_t
  {
  };

  struct adopt_lock_t
  {
  };

  constexpr defer_lock_t defer_lock{};
  constexpr try_to_lock_t try_to_lock{};
  constexpr adopt_lock_t adopt_lock{};

  template <typename MutexT>
  class lock_guard
  {
  public:
    using mutex_type = MutexT;

    explicit lock_guard(mutex_type& mtx) : m_mutex(mtx) { m_mutex.lock(); }
    lock_guard(mutex_type& mtx, adopt_lock_t) : m_mutex(mtx) {}

    ~lock_guard() { m_mutex.unlock(); }

    lock_guard(const lock_guard&) = delete;
    lock_guard& operator=(const lock_guard&) = delete;

  private:
    mutex_type& m_mutex;
  };

  template <typename MutexT>
  class unique_lock
  {
  public:
    using mutex_type = MutexT;

    unique_lock() noexcept : m_mutex(0), m_is_owner(false) {}

    explicit unique_lock(mutex_type& mtx) : m_mutex(&mtx), m_is_owner(false)
    {
      lock();
      m_is_owner = true;
    }

    unique_lock(mutex_type& mtx, defer_lock_t) noexcept : m_mutex(&mtx), m_is_owner(false) {}
    unique_lock(mutex_type& mtx, try_to_lock_t) : m_mutex(&mtx), m_is_owner(m_mutex->try_lock()) {}
    unique_lock(mutex_type& mtx, adopt_lock_t) : m_mutex(&mtx), m_is_owner(true) {}

    template <typename ClockT, typename DurationT>
    unique_lock(mutex_type& mtx, const time::time_point<ClockT, DurationT>& atime) : m_mutex(&mtx), m_is_owner(m_mutex->try_lock_until(atime))
    {
    }

    template <typename Rep, typename Period>
    unique_lock(mutex_type& mtx, const time::duration<Rep, Period>& rtime) : m_mutex(&mtx), m_is_owner(m_mutex->try_lock_for(rtime))
    {
    }

    ~unique_lock()
    {
      if (m_is_owner) unlock();
    }

    unique_lock(const unique_lock&) = delete;
    unique_lock& operator=(const unique_lock&) = delete;

    unique_lock(unique_lock&& other) noexcept : m_mutex(other.m_mutex), m_is_owner(other.m_is_owner)
    {
      other.m_mutex = 0;
      other.m_is_owner = false;
    }

    unique_lock& operator=(unique_lock&& other) noexcept
    {
      if (m_is_owner) unlock();

      unique_lock(std::move(other)).swap(*this);

      other.m_mutex = 0;
      other.m_is_owner = false;

      return *this;
    }

    void lock()
    {
      if (!m_mutex)
        throw std::system_error(std::make_error_code(std::errc::operation_not_permitted));
      else if (m_is_owner)
        throw std::system_error(std::make_error_code(std::errc::resource_deadlock_would_occur));
      else
      {
        m_mutex->lock();
        m_is_owner = true;
      }
    }

    bool try_lock()
    {
      if (!m_mutex)
        throw std::system_error(std::make_error_code(std::errc::operation_not_permitted));
      else if (m_is_owner)
        throw std::system_error(std::make_error_code(std::errc::resource_deadlock_would_occur));
      else
      {
        m_is_owner = m_mutex->try_lock();
        return m_is_owner;
      }
    }

    template <typename ClockT, typename DurationT>
    bool try_lock_until(const time::time_point<ClockT, DurationT>& atime)
    {
      if (!m_mutex)
        throw std::system_error(std::make_error_code(std::errc::operation_not_permitted));
      else if (m_is_owner)
        throw std::system_error(std::make_error_code(std::errc::resource_deadlock_would_occur));
      else
      {
        m_is_owner = m_mutex->try_lock_until(atime);
        return m_is_owner;
      }
    }

    template <typename Rep, typename Period>
    bool try_lock_for(const time::duration<Rep, Period>& rtime)
    {
      if (!m_mutex)
        throw std::system_error(std::make_error_code(std::errc::operation_not_permitted));
      else if (m_is_owner)
        throw std::system_error(std::make_error_code(std::errc::resource_deadlock_would_occur));
      else
      {
        m_is_owner = m_mutex->try_lock_for(rtime);
        return m_is_owner;
      }
    }

    void unlock()
    {
      if (!m_mutex)
        throw std::system_error(std::make_error_code(std::errc::operation_not_permitted));
      else if (m_mutex)
      {
        m_mutex->unlock();
        m_is_owner = false;
      }
    }

    void swap(unique_lock& other) noexcept
    {
      std::swap(m_mutex, other.m_mutex);
      std::swap(m_is_owner, other.m_is_owner);
    }

    mutex_type* release() noexcept
    {
      mutex_type* __ret = m_mutex;
      m_mutex = nullptr;
      m_is_owner = false;
      return __ret;
    }

    bool owns_lock() const noexcept { return m_is_owner; }
    explicit operator bool() const noexcept { return owns_lock(); }

    mutex_type* mutex() const noexcept { return m_mutex; }

  private:
    mutex_type* m_mutex;
    bool m_is_owner;
  };

  template <typename MutexT>
  inline void swap(unique_lock<MutexT>& lhs, unique_lock<MutexT>& rhs) noexcept
  {
    lhs.swap(rhs);
  }

  namespace detail
  {
    template <int Index>
    struct unlock_impl
    {
      template <typename... LockT>
      static void do_unlock(std::tuple<LockT&...>& lock_tuple)
      {
        std::get<Index>(lock_tuple).unlock();
        unlock_impl<Index - 1>::do_unlock(lock_tuple);
      }
    };

    template <>
    struct unlock_impl<-1>
    {
      template <typename... LockT>
      static void do_unlock(std::tuple<LockT&...>&)
      {
      }
    };

    template <typename LockT>
    unique_lock<LockT> try_to_lock(LockT& lock)
    {
      return unique_lock<LockT>(lock, try_to_lock);
    }

    template <int Index, bool Continue = true>
    struct try_lock_impl
    {
      template <typename... LockT>
      static void do_try_lock(std::tuple<LockT&...>& lock_tuple, int& idx)
      {
        idx = Index;
        auto lock = try_to_lock(std::get<Index>(lock_tuple));
        if (lock.owns_lock())
        {
          try_lock_impl<Index + 1, Index + 2 < sizeof...(LockT)>::do_try_lock(lock_tuple, idx);
          if (idx == -1) lock.release();
        }
      }
    };

    template <int Index>
    struct try_lock_impl<Index, false>
    {
      template <typename... LockT>
      static void do_try_lock(std::tuple<LockT&...>& lock_tuple, int& idx)
      {
        idx = Index;
        auto lock = try_to_lock(std::get<Index>(lock_tuple));
        if (lock.owns_lock())
        {
          idx = -1;
          lock.release();
        }
      }
    };

  } // namespace detail

  template <typename _Lock1, typename _Lock2, typename... _Lock3>
  int try_lock(_Lock1& __l1, _Lock2& __l2, _Lock3&... __l3)
  {
    int idx;
    auto lock_tuple = std::tie(__l1, __l2, __l3...);
    detail::try_lock_impl<0>::do_try_lock(lock_tuple, idx);
    return idx;
  }

  template <typename _L1, typename _L2, typename... _L3>
  void lock(_L1& __l1, _L2& __l2, _L3&... __l3)
  {
    while (true)
    {
      unique_lock<_L1> __first(__l1);
      int idx;
      auto lock_tuple = std::tie(__l2, __l3...);
      detail::try_lock_impl<0, sizeof...(_L3)>::do_try_lock(lock_tuple, idx);
      if (idx == -1)
      {
        __first.release();
        return;
      }
    }
  }

  //   /// once_flag
  //   struct once_flag
  //   {
  //   private:
  //     typedef __gthread_once_t native_type;
  //     native_type _M_once = __GTHREAD_ONCE_INIT;

  //   public:
  //     /// Constructor
  //     constexpr once_flag() noexcept = default;

  //     /// Deleted copy constructor
  //     once_flag(const once_flag&) = delete;
  //     /// Deleted assignment operator
  //     once_flag& operator=(const once_flag&) = delete;

  //     template <typename _Callable, typename... _Args>
  //     friend void call_once(once_flag& __once, _Callable&& __f, _Args&&... __args);
  //   };

  // #ifdef _GLIBCXX_HAVE_TLS
  //   extern __thread void* __once_callable;
  //   extern __thread void (*__once_call)();

  //   template <typename _Callable>
  //   inline void __once_call_impl()
  //   {
  //     (*(_Callable*)__once_callable)();
  //   }
  // #else
  //   extern function<void()> __once_functor;

  //   extern void __set_once_functor_lock_ptr(unique_lock<mutex>*);

  //   extern mutex& __get_once_mutex();
  // #endif

  //   extern "C" void __once_proxy(void);

  //   /// call_once
  //   template <typename _Callable, typename... _Args>
  //   void call_once(once_flag& __once, _Callable&& __f, _Args&&... __args)
  //   {
  // #ifdef _GLIBCXX_HAVE_TLS
  //     auto __bound_functor = std::__bind_simple(std::forward<_Callable>(__f), std::forward<_Args>(__args)...);
  //     __once_callable = &__bound_functor;
  //     __once_call = &__once_call_impl<decltype(__bound_functor)>;
  // #else
  //     unique_lock<mutex> __functor_lock(__get_once_mutex());
  //     auto __callable = std::__bind_simple(std::forward<_Callable>(__f), std::forward<_Args>(__args)...);
  //     __once_functor = [&]() { __callable(); };
  //     __set_once_functor_lock_ptr(&__functor_lock);
  // #endif

  //     int __e = __gthread_once(&__once._M_once, &__once_proxy);

  // #ifndef _GLIBCXX_HAVE_TLS
  //     if (__functor_lock) __set_once_functor_lock_ptr(0);
  // #endif

  //     if (__e) __throw_system_error(__e);
  //   }

  // #endif // _GLIBCXX_USE_C99_STDINT_TR1

  // #endif // C++11
} // namespace wge

#endif // WGE_THREAD_PSP_MUTEX_HXX