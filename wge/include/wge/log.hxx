#ifndef WGE_LOG_HXX
#define WGE_LOG_HXX

#include <wge/log/formatter.hxx>
#include <wge/types.hxx>

#include <chrono>
#include <cstdio>

namespace wge
{
  namespace log
  {
    enum class category : wge::uint8_t
    {
      general,
      app,
      engine,
      files,
      error,
      assert,
      profile,
      test
    };

    inline std::string to_string(category c) noexcept
    {
      std::string category_string("app");
      switch (c)
      {
        case category::general:
          category_string = "general";
          break;
        case category::app:
          category_string = "app";
          break;
        case category::engine:
          category_string = "engine";
          break;
        case category::files:
          category_string = "files";
          break;
        case category::error:
          category_string = "error";
          break;
        case category::assert:
          category_string = "assert";
          break;
        case category::profile:
          category_string = "profile";
          break;
        case category::test:
          category_string = "test";
          break;
      }
      return category_string;
    }
    enum class level : wge::uint8_t
    {
      trace,
      debug,
      info,
      warning,
      error,
      fatal
    };

    inline std::string to_string(level l) noexcept
    {
      std::string level_string("info");
      switch (l)
      {
        case level::trace:
          level_string = "trace";
          break;
        case level::debug:
          level_string = "debug";
          break;
        case level::info:
          level_string = "info";
          break;
        case level::warning:
          level_string = "warning";
          break;
        case level::error:
          level_string = "error";
          break;
        case level::fatal:
          level_string = "fatal";
          break;
      }
      return level_string;
    }
  } // namespace log
} // namespace wge

inline std::ostream& operator<<(std::ostream& os, const wge::log::category& c) noexcept
{
  os << wge::log::to_string(c);
  return os;
}

inline std::ostream& operator<<(std::ostream& os, const wge::log::level& l) noexcept
{
  os << wge::log::to_string(l);
  return os;
}

namespace wge
{
  namespace log
  {
    struct timestamp final
    {
      using clock_type = std::chrono::system_clock;
      using time_point_type = std::chrono::time_point<clock_type>;

      timestamp() noexcept : time_point(clock_type::now()) {}

      explicit timestamp(const time_point_type& tp) noexcept : time_point(tp) {}
      explicit timestamp(time_point_type&& tp) noexcept : time_point(std::move(tp)) {}

      inline void swap(timestamp& other) noexcept
      {
        auto tmp = time_point;
        time_point = other.time_point;
        other.time_point = tmp;
      }

      time_point_type time_point;
    };

    inline void swap(timestamp& lhs, timestamp& rhs) noexcept { lhs.swap(rhs); }

    inline std::string to_string(const timestamp& ts, const std::string& format = "%Y-%m-%d %H-%M-%S.#ms") noexcept
    {
      std::time_t now_c = timestamp::clock_type::to_time_t(ts.time_point);
      struct ::tm* now_tm = std::localtime(&now_c);
      char buffer[256];
      strftime(buffer, sizeof buffer, format.c_str(), now_tm);
      std::string result(buffer);
      size_t pos = result.find("#ms");
      if (pos != std::string::npos)
      {
        int ms_part = std::chrono::time_point_cast<std::chrono::milliseconds>(ts.time_point).time_since_epoch().count() % 1000;
        char ms_str[4];
        sprintf(&ms_str[0], "%03d", ms_part);
        result.replace(pos, 3, ms_str);
      }
      return result;
    }

    class message
    {
    public:
      message() noexcept : m_level(level::trace), m_category(category::general), m_message(), m_timestamp() {}

      message(level l, const std::string& msg) : m_level(l), m_category(category::general), m_message(msg) {}
      message(level l, category c, const std::string& msg) : m_level(l), m_category(c), m_message(msg) {}

      message(const message& msg) : m_level(msg.m_level), m_category(msg.m_category), m_message(msg.m_message), m_timestamp(msg.m_timestamp) {}
      message(message&& msg)
          : m_level(std::move(msg.m_level)), m_category(std::move(msg.m_category)), m_message(std::move(msg.m_message)), m_timestamp(std::move(msg.m_timestamp))
      {
      }

      inline message& operator=(const message& other)
      {
        message(other).swap(*this);
        return *this;
      }

      inline message& operator=(message&& other)
      {
        message(std::move(other)).swap(*this);
        return *this;
      }

      inline level get_level() const noexcept { return m_level; }
      inline category get_category() const noexcept { return m_category; }
      inline std::string get_message() const noexcept { return m_message; }
      inline timestamp get_timestamp() const noexcept { return m_timestamp; }

      inline void swap(message& rhs) noexcept
      {
        using std::swap;
        swap(m_level, rhs.m_level);
        swap(m_category, rhs.m_category);
        swap(m_message, rhs.m_message);
        swap(m_timestamp, rhs.m_timestamp);
      }

      inline bool equals(const message& other) const noexcept
      {
        return (m_level == other.m_level) && (m_message == other.m_message) && (m_category == other.m_category);
      }

    private:
      level m_level;
      category m_category;
      std::string m_message;
      timestamp m_timestamp;
    };

    inline std::string to_string(const message& msg) noexcept
    {
      return std::string{to_string(msg.get_timestamp()) + " [" + to_string(msg.get_level()) + "]: " + msg.get_message()};
    }

    inline bool operator==(const message& lhs, const message& rhs) { return lhs.equals(rhs); }
    inline bool operator!=(const message& lhs, const message& rhs) { return !(lhs == rhs); }

    namespace detail
    {
      template <typename... Args>
      static inline message create_message(const char* file, const char* func, int line, const std::string& fmt, const Args&... args)
      {
        return message{wge::log::level::info, wge::log::detail::create_function_prefix(file, func, line) + wge::log::format(fmt, args...)};
      }

      template <typename... Args>
      static inline message create_message(const char* file, const char* func, int line, level level, const std::string& fmt, const Args&... args)
      {
        return message{level, wge::log::detail::create_function_prefix(file, func, line) + wge::log::format(fmt, args...)};
      }

      template <typename... Args>
      static inline message
      create_message(const char* file, const char* func, int line, category channel, level level, const std::string& fmt, const Args&... args)
      {
        return message{level, channel, wge::log::detail::create_function_prefix(file, func, line) + wge::log::format(fmt, args...)};
      }
    } // namespace detail

    class static_logger final
    {
      struct log_creater final
      {
      public:
        log_creater() : m_message() {}
        log_creater(log_creater&&) = default;
        log_creater(const log_creater&) = delete;

        ~log_creater() { static_logger::log(message(m_level, m_category, m_message)); }

        log_creater& operator<<(category c)
        {
          m_category = c;
          return *this;
        }

        log_creater& operator<<(level p)
        {
          m_level = p;
          return *this;
        }

        template <typename T>
        log_creater& operator<<(T&& entry)
        {
          std::ostringstream ss;
          ss << std::forward<T>(entry);
          m_message.append(ss.str());

          return *this;
        }

        template <typename T>
        log_creater& operator<<(const T& entry)
        {
          std::ostringstream ss;
          ss << entry;
          m_message.append(ss.str());

          return *this;
        }

      private:
        std::string m_message;
        category m_category = category::app;
        level m_level = level::info;
      };

    public:
      static inline void log(const message& msg) { std::cout << to_string(msg) << std::endl << std::flush; }
      static inline log_creater out() { return log_creater{}; }

    private:
      static_logger() = delete;
      ~static_logger() = delete;

      static_logger(const static_logger&) = delete;
      static_logger& operator=(const static_logger&) = delete;

      static_logger(static_logger&&) = delete;
      static_logger& operator=(static_logger&&) = delete;
    };

  } // namespace log
} // namespace wge

#define WGE_LOG() ::wge::log::static_logger::out() << ::wge::log::detail::create_function_prefix(__FILE__, __FUNCTION__, __LINE__)
#define WGE_PRINTF(...) ::wge::log::static_logger::log(::wge::log::detail::create_message(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__));

#endif /*WGE_LOG_HXX*/