#include <wge/memory.hxx>

#include <catch.hpp>

TEST_CASE("test owner type", "[wge][memory]")
{
  SECTION("test pointer access")
  {
    struct util
    {
      static void increment(int* i) { *i += 1; }
    };

    wge::owner_ptr<int*> ptr = new int(42);
    REQUIRE(ptr != nullptr);
    REQUIRE(*ptr == 42);

    util::increment(ptr);
    REQUIRE(*ptr == 43);

    delete ptr;
  }

  SECTION("test nullptr compare")
  {
    wge::owner_ptr<int*> ptr = nullptr;
    REQUIRE(ptr == nullptr);
  }
}
