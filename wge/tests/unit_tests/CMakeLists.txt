set(PROJECT_TEST_NAME wge_tests)

include(ParseAndAddCatchTests)

add_executable (${PROJECT_TEST_NAME}    ${CMAKE_CURRENT_SOURCE_DIR}/memory_tests.cxx
                                        ${CMAKE_CURRENT_SOURCE_DIR}/log/formatter_tests.cxx
)

target_link_libraries (${PROJECT_TEST_NAME} PUBLIC WGE::WGE Catch2::catch_static)
set_target_properties(${PROJECT_TEST_NAME} PROPERTIES CXX_STANDARD 11 CXX_STANDARD_REQUIRED YES)

ParseAndAddCatchTests (${PROJECT_TEST_NAME})