#ifndef PRECOMPILEDHEADER_H
#define PRECOMPILEDHEADER_H

#include <algorithm>
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "DebugRoutines.h"
#include "config.h"

#include <assert.h>

#include "JFileSystem.h"
#include "JGE.h"
#include "JLogger.h"

#include "GameOptions.h"

#ifndef WP8
#include <wge/memory.hxx>
#endif

#if defined(WP8) || defined(IOS) || defined(ANDROID) || defined(QT_CONFIG) || defined(SDL_CONFIG)
#define TOUCH_ENABLED
#endif

#endif // PRECOMPILEDHEADER_H
